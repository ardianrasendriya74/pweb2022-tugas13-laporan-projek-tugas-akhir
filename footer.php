<link rel="stylesheet" href="style.css">
<script src="fontawesome/js/all.min.js"></script>
<footer>
      <div class="main-content">
        <div class="left box">
          <h2>Tentang Kami</h2>
          <div class="content">
            <p>MyShop hadir sebagai bentuk apresiasi kepada seluruh pelanggan setia. MyShop yang bekerja sama dengan LinkAja! dalam mendukung sistem pembayaran agar mempermudah pelanggan saat bertransaksi. MyShop adalah aplikasi yang mudah diakses dengan banyak keuntngan.</p>
            <div class="social">
              <a href="https://facebook.com/"><span class="fab fa-facebook-f"></span></a>
              <a href="#"><span class="fab fa-twitter"></span></a>
              <a href="https://instagram.com/"><span class="fab fa-instagram"></span></a>
              <a href="https://youtube.com/c/"><span class="fab fa-youtube"></span></a>
            </div>
          </div>
        </div>
        <div class="center box">
          <h2>Alamat</h2>
          <div class="content">
            <div class="place">
              <span class="fas fa-map-marker-alt"></span>
              <span class="text">Bantul, Yogyakarta</span>
            </div>
            <div class="phone">
              <span class="fas fa-phone-alt"></span>
              <span class="text">+6285727525160</span>
            </div>
            <div class="email">
              <span class="fas fa-envelope"></span>
              <span class="text">ardiyanrasendriya@gmail.com</span>
            </div>
          </div>
        </div>
        <div class="right box">
          <h2>Kontak Kami</h2>
          <div class="content">
            <form action="#">
              <div class="email">
                <div class="text">Email *</div>
                <input type="email" required>
              </div>
              <div class="msg">
                <div class="text">Pesan *</div>
                <textarea id=".msgForm" rows="2" cols="25" required></textarea><br>

              <div class="btn">
                <button class="btn btn-success" type="submit">Kirim</button>
              </div>
            </form>
          </div>
          <div class="bottom">
	        <center>
	          <span class="credit">Created By <a href="https://www.youtube.com/channel/UCOxjT7-ACbTp0q1HpVQyCeg">Ardiyan Rasendriya</a> </span>
	          <span class="far fa-copyright"></span> 2022 All rights reserved.
	        </center>
	      </div>
	      
     